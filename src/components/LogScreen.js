import React, { useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet } from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

const LogScreen = () => {
  const navigation = useNavigation();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleLogin = async () => {
    try {
      const response = await axios.get('http://10.0.2.2:3000/users');

      const users = response.data;
      const user = users.find((u) => u.username === username && u.password === password);

      if (user) {
        navigation.navigate('TabNavigator'); 
      } else {
        alert('Nieprawidłowy login lub hasło');
      }
    } catch (error) {
      console.error('Błąd logowania', error);
    }
  };

  return (
    <View style={styles.container}>
      <Text>Login:</Text>
      <TextInput
        style={styles.input}
        onChangeText={setUsername}
        value={username}
        placeholder="Wprowadź login"
      />
      <Text>Hasło:</Text>
      <TextInput
        style={styles.input}
        onChangeText={setPassword}
        value={password}
        placeholder="Wprowadź hasło"
        secureTextEntry
      />
      <Button title="Zaloguj się" onPress={handleLogin} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 20,
    padding: 10,
    width: 200,
  },
});

export default LogScreen;
