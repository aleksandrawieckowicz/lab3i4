
import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import TabNavigator from './TabNavigator';
import LogScreen from './LogScreen';


const Stack = createStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator initialRouteName="LogScreen"  screenOptions={{
      headerStyle: {
        
        height: 120,
      },
    }}>
      <Stack.Screen name="LogScreen" component={LogScreen} />
      <Stack.Screen name="TabNavigator" component={TabNavigator} />
    </Stack.Navigator>
  );
};

export default StackNavigator;
