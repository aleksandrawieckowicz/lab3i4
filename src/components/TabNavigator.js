import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import AllTransactScreen from './AllTrasactScreen';
import PaidTransactScreen from './PaidTransactScreen';
import UnpaidTransactScreen from './UnpaidTransactScreen';

const Tab = createBottomTabNavigator();

const TabNavigator = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name="LIST" component={AllTransactScreen} />
      <Tab.Screen name="PAID" component={PaidTransactScreen} />
      <Tab.Screen name="UNPAID" component={UnpaidTransactScreen} />
    </Tab.Navigator>
  );
};

export default TabNavigator;
