import React, { useState, useEffect } from 'react';
import { View, FlatList, Text, Button, StyleSheet } from 'react-native';
import axios from 'axios';
import { useFocusEffect } from '@react-navigation/native';

const API_URL = 'http://10.0.2.2:3000/orders';

const PaidTransactScreen = () => {
    const [orders, setOrders] = useState([]);

    useEffect(() => {
        fetchOrders();
    }, []);

    useFocusEffect(
        React.useCallback(() => {
          fetchOrders();
        }, [])
      );
    

    const fetchOrders = async () => {
        try {
            const response = await axios.get(API_URL);
            setOrders(response.data);
        } catch (error) {
            console.error('Błąd pobierania zamówień:', error);
        }
    };

    const hasPaidOrders = orders.some(order => order.status === 'zapłacony');

    if (!hasPaidOrders) {
        return <Text>Brak zapłaconych zamówień</Text>;
    }

    const renderItem = ({ item }) => (
        <>
            {item.status === 'zapłacony' ? (
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 10 }}>
                    <Text>{item.name}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={styles.txt}>
                            <Text>{item.status}</Text>
                        </View>
                    </View>
                </View>
            ) : null}
        </>
    );

    return (
        <FlatList
            data={orders}
            renderItem={renderItem}
            keyExtractor={(item) => item.id.toString()}
        />
    );
};

export default PaidTransactScreen;

const styles = StyleSheet.create({
    txt: {
        marginRight: 50
    }
});
